# brightnessUI

## v1.2 (2024-2-15)

Improve folder structure.

## v1.1 (2024-2-15)

Modify long description to display README.md.

## v1.0 (2024-2-15)

Basic functions to tune screen backlight by modifying `/sys/class/backlight/xxx/brightness` directly, where `xxx` differs according to your computer.
